package file_manager.manager.position_manager;

import file_manager.file_objects.file_objects_types.directory.Directory;
import file_manager.file_objects.file_objects_types.root.Root;
import file_manager.file_parts.utilz.folder_model_creator.FolderModelCreator;
import file_manager.object_forms.abstractions.file_object_abstract.folders.FolderAbstract;

import static file_manager.static_fields.file_objects_fields.GeneralFileObjectsFields.ROOT_PARENT;


public class PositionManager {

    private FolderAbstract currentFileModelAbstract;
    private final FolderAbstract root;

    public PositionManager(Root root){
        this.root = root;
    }


    public FolderAbstract getCurrentFolderModel() {
        return currentFileModelAbstract;
    }

    public void setCurrentFileModelAbstract(FolderAbstract currentFileModelAbstract) {
        this.currentFileModelAbstract = currentFileModelAbstract;
    }

    public FolderAbstract getPreviousFolderModelAbstract() throws Exception {
        String parentAbsolutePath = currentFileModelAbstract.close();
        if (checkIfRoot(parentAbsolutePath)){
            return currentFileModelAbstract;
        }else{
            String absolutePathOfPreviousFolderModel = currentFileModelAbstract.getFileInformationHolder().getStringFileRepresenter().getParentAbsoultePath();
            System.out.println("ovaj path mora biti ROOT = "+currentFileModelAbstract.getFileInformationHolder().getStringFileRepresenter().getParentAbsoultePath());
            String parent = null;
            return FolderModelCreator.getFileHandler().createFolderModel(absolutePathOfPreviousFolderModel);
        }
    }
    private boolean checkIfRoot(String absolutePath)throws Exception{
        try{
            System.out.println("CHECKING IF ROOT "+absolutePath);
            return absolutePath.equals(ROOT_PARENT);
        }catch (Exception e){
            throw new Exception("this is ROOT_PARENT");
        }
    }

    public void swapCurrentWithNewFileModelAbstract(String newAbsolutePath) {
        try{
            Directory newCurrent = (Directory) FolderModelCreator.getFileHandler().createFolderModel(newAbsolutePath);
            currentFileModelAbstract = newCurrent;
        }catch (Exception e){
            if (e.getMessage().contains("is not folder")){
                System.out.println("Is not folder");
            }else{
                e.printStackTrace();
            }
        }
    }

    public void reset(){
        currentFileModelAbstract = root;
    }

}
