package file_manager.manager;

import file_manager.file_objects.file_objects_actions.directory_actions.DirectoryActions;
import file_manager.file_objects.file_objects_actions.root_actions.RootActions;
import file_manager.file_objects.file_objects_types.root.Root;
import file_manager.file_parts.utilz.string_represent_file_handler.string_creator.StringFilesCreator;
import file_manager.file_parts.utilz.string_represent_file_handler.string_file_representer.StringFileRepresenter;
import file_manager.manager.position_manager.PositionManager;
import file_manager.object_forms.abstractions.file_object_abstract.folders.FolderAbstract;
import file_manager.object_forms.models.user_implementation_models.root.RootInitializationModel;
import file_manager.static_fields.file_objects_fields.FileTypeFields;

import java.io.File;

public class FileManager {

    private PositionManager positionManager;
    private Root root;

    public FileManager(){
    }

    public void initFileManager(RootInitializationModel rootInitializationModel) throws Exception {
        root = (Root) RootActions.getRootActions().init(rootInitializationModel);
        positionManager = new PositionManager(root);
        positionManager.setCurrentFileModelAbstract(root);
    }

    public FolderAbstract getActiveFolderModel() {
        return positionManager.getCurrentFolderModel();

    }

    public FolderAbstract getPreviousFolderModel() throws Exception {
        return positionManager.getPreviousFolderModelAbstract();
    }

    public void clickedOnFileModel(String fileName) throws Exception {
        DirectoryActions directoryActions = DirectoryActions.getDirectoryActions();
        String newAbsolutePath = positionManager.getCurrentFolderModel().open(fileName);

        if (directoryActions.checkIfDirectory(newAbsolutePath)){
            positionManager.swapCurrentWithNewFileModelAbstract(newAbsolutePath);
        }
    }
    public void goBackToParentFileModel(){
        try{
            String parentAbsolutePath = positionManager.getPreviousFolderModelAbstract().getFileInformationHolder().getStringFileRepresenter().getAbsolutePath();
            positionManager.swapCurrentWithNewFileModelAbstract(parentAbsolutePath);
        }catch (Exception e){
            if (e.getMessage().equals("this is ROOT_PARENT")){
                positionManager.reset();
            }
        }
    }
    public String getFileProperties(String fileName){
        StringFileRepresenter[] currentFilesAndFolders = positionManager.getCurrentFolderModel().getAllFilesAndFolders();

        StringFileRepresenter fileRepresenter;
        for (StringFileRepresenter stringFileRepresenter : currentFilesAndFolders){
            if (stringFileRepresenter.getCleanFileName().equals(fileName)){
                return StringFilesCreator.getStringFilesCreator().createStringFromFileProperties(stringFileRepresenter);
            }
        }
        return "No properties";
    }

    public String currentFilesAndFoldersToString(){
        return StringFilesCreator.getStringFilesCreator().createOneStringOfFilesName(getActiveFolderModel().getAllFilesAndFolders());
    }
    public String checkFileType(String fileName){
        if (fileName.matches(FileTypeFields.picture)){
            return "picture";
        }else if (fileName.matches(FileTypeFields.music)){
            return "music";
        }else if (fileName.matches(FileTypeFields.text)){
            return "text";
        }
        return "unknown";
    }

    public File getFileObjectFromCurrentActiveFolder(String fileName){
        StringFileRepresenter[] currentFilesAndFolders = positionManager.getCurrentFolderModel().getAllFilesAndFolders();

        StringFileRepresenter fileRepresenter;
        for (StringFileRepresenter stringFileRepresenter : currentFilesAndFolders){
            if (stringFileRepresenter.getCleanFileName().equals(fileName)){
                return new File(stringFileRepresenter.getAbsolutePath());
            }
        }
        return null;
    }



    public void testFunction(FolderAbstract directory) throws Exception {
        Root root = (Root) RootActions.getRootActions().init(new RootInitializationModel() {
            @Override
            public File[] initializationOfRootFiles() {
                return File.listRoots();
            }

            @Override
            public String getRootIdentification() {
                return null;
            }
        });
        positionManager = new PositionManager(root);
        positionManager.setCurrentFileModelAbstract(directory);
    }

}
