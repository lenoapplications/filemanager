package file_manager.object_forms.abstractions.file_object_abstract.folders;

import file_manager.file_parts.holders.file_information_holder.FileInformationHolder;
import file_manager.file_parts.utilz.string_represent_file_handler.string_file_representer.StringFileRepresenter;
import file_manager.object_forms.models.file_object_models.folders.FoldersModel;


public abstract class FolderAbstract implements FoldersModel{
    protected final FileInformationHolder fileInformationHolder;

    public FolderAbstract(StringFileRepresenter stringFileRepresenter){
        fileInformationHolder = new FileInformationHolder(stringFileRepresenter);
    }

    @Override
    public String close() {
        return fileInformationHolder.getStringFileRepresenter().getParentAbsoultePath();

    }

    public FileInformationHolder getFileInformationHolder() {
        return fileInformationHolder;
    }


    protected StringFileRepresenter fillInStringFileRepresenter(String fileName,String absolutePath,String parentAbsolutePath){
        //OVA METODA VEC POSTOJI KOD FILEMODLECREATORA
        StringFileRepresenter stringFileRepresenter = new StringFileRepresenter();
        stringFileRepresenter.setAbsolutePathString(absolutePath);
        stringFileRepresenter.setParentAbsoultePath(parentAbsolutePath);

        return stringFileRepresenter;
    }


}
