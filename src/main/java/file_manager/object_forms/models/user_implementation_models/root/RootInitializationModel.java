package file_manager.object_forms.models.user_implementation_models.root;


import java.io.File;

public interface RootInitializationModel {
    File[] initializationOfRootFiles();
    String getRootIdentification();
}
