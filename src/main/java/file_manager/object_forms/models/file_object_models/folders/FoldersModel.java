package file_manager.object_forms.models.file_object_models.folders;

import file_manager.file_parts.utilz.string_represent_file_handler.string_file_representer.StringFileRepresenter;


public interface FoldersModel {
    String open(String directoryToOpen) throws Exception;
    StringFileRepresenter[] getAllFilesAndFolders();
    String close();
}
