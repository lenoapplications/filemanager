package file_manager.static_fields.file_objects_fields;

public class GeneralFileObjectsFields {
    public static final String ROOT_NAME = "ROOT";
    public static final String ROOT_PATH = ".ROOT";
    public static final String ROOT_PARENT = "NO PARENT";
}
