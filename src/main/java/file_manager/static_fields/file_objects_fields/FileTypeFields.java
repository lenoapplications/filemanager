package file_manager.static_fields.file_objects_fields;

public class FileTypeFields {
    public static final String picture = ".+\\.(png|jpeg|jpg)";
    public static final String music = ".+\\.(mp3|mp4|wav|wma)";
    public static final String text = ".+\\.(txt|java|cpp|py|docx)";

}
