package file_manager.file_objects.file_objects_actions.directory_actions;




import java.io.File;

public class DirectoryActions {
    private static DirectoryActions directoryActions;

    private DirectoryActions(){

    }
    public static DirectoryActions getDirectoryActions(){
        if (directoryActions == null){
            directoryActions = new DirectoryActions();
        }
        return directoryActions;
    }

    public boolean checkIfDirectory(String absolutePath){
        System.out.println(new File(absolutePath).isDirectory());
        return new File(absolutePath).isDirectory();
    }

}
