package file_manager.file_objects.file_objects_actions.root_actions.initialization_action;

import file_manager.file_objects.file_objects_types.root.Root;
import file_manager.file_parts.utilz.string_represent_file_handler.string_file_representer.StringFileRepresenter;
import file_manager.object_forms.abstractions.file_object_abstract.folders.FolderAbstract;
import file_manager.object_forms.models.user_implementation_models.root.RootInitializationModel;

import java.io.File;

import static file_manager.static_fields.file_objects_fields.GeneralFileObjectsFields.ROOT_NAME;
import static file_manager.static_fields.file_objects_fields.GeneralFileObjectsFields.ROOT_PARENT;
import static file_manager.static_fields.file_objects_fields.GeneralFileObjectsFields.ROOT_PATH;

public class RootInitializationActions {

    public FolderAbstract initializingRootFilesAction(RootInitializationModel rootInitializationModel) throws Exception{
        return wrapToRootObject(rootInitializationModel);
    }

    private FolderAbstract wrapToRootObject(RootInitializationModel rootInitializationModel){
        StringFileRepresenter stringFileRepresenter = new StringFileRepresenter();
        stringFileRepresenter.setAbsolutePathString(ROOT_PATH);
        stringFileRepresenter.setParentAbsoultePath(ROOT_PARENT);

        return new Root(stringFileRepresenter,rootInitializationModel);
    }
}
