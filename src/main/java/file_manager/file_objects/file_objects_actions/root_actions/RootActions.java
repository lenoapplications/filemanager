package file_manager.file_objects.file_objects_actions.root_actions;

import file_manager.file_objects.file_objects_actions.root_actions.initialization_action.RootInitializationActions;
import file_manager.file_objects.file_objects_types.root.Root;
import file_manager.object_forms.abstractions.file_object_abstract.folders.FolderAbstract;
import file_manager.object_forms.models.user_implementation_models.root.RootInitializationModel;



public class RootActions {
    private static RootActions rootActions;
    private final RootInitializationActions rootInitializationActions;


    private RootActions(){
        rootInitializationActions = new RootInitializationActions();
    }

    public static RootActions getRootActions(){
        if (rootActions == null){
            rootActions = new RootActions();
        }
        return rootActions;
    }

    public FolderAbstract init(RootInitializationModel rootInitializationModel) throws Exception {
        Root.rootIdentification = rootInitializationModel.getRootIdentification();
        return rootInitializationActions.initializingRootFilesAction(rootInitializationModel);
    }



}
