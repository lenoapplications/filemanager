package file_manager.file_objects.file_objects_types.directory;

import file_manager.file_parts.utilz.string_represent_file_handler.string_file_representer.StringFileRepresenter;
import file_manager.object_forms.abstractions.file_object_abstract.folders.FolderAbstract;

import java.io.File;

public class Directory extends FolderAbstract {

    public Directory(StringFileRepresenter stringFileRepresenter) {
        super(stringFileRepresenter);
    }

    @Override
    public String open(String directory) {
        String oldAbsolutePath = fileInformationHolder.getStringFileRepresenter().getAbsolutePath();
        return new File(oldAbsolutePath,directory).getAbsolutePath();
    }

    @Override
    public StringFileRepresenter[] getAllFilesAndFolders() {
        File thisFolder = new File(fileInformationHolder.getStringFileRepresenter().getAbsolutePath());
        File[] listOfFiles = thisFolder.listFiles();

        int size = (listOfFiles == null) ? 0 : listOfFiles.length;
        StringFileRepresenter[] stringFileRepresenter = new StringFileRepresenter[size];

        for (int i = 0; i < size; i++){
            stringFileRepresenter[i] = fillInStringFileRepresenter(listOfFiles[i].getName(),listOfFiles[i].getAbsolutePath(),listOfFiles[i].getParent());
        }
        return stringFileRepresenter;
    }
}
