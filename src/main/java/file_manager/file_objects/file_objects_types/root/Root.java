package file_manager.file_objects.file_objects_types.root;

import file_manager.file_parts.utilz.string_represent_file_handler.string_file_representer.StringFileRepresenter;
import file_manager.object_forms.abstractions.file_object_abstract.folders.FolderAbstract;
import file_manager.object_forms.models.user_implementation_models.root.RootInitializationModel;

import java.io.File;


public class Root extends FolderAbstract {
    private final RootInitializationModel rootInitializationModel;
    public static String rootIdentification;

    public Root(StringFileRepresenter stringFileRepresenter, RootInitializationModel rootInitializationModel){
        super(stringFileRepresenter);
        this.rootInitializationModel = rootInitializationModel;
    }

    @Override
    public String open(String directorToOpen) throws Exception {
        if (searchForMatchDirectoryToOpen(directorToOpen)){
            return directorToOpen;
        }
        return null;
    }

    @Override
    public StringFileRepresenter[] getAllFilesAndFolders() {
        File[] listOfFiles = rootInitializationModel.initializationOfRootFiles();
        StringFileRepresenter[] stringFileRepresenters = new StringFileRepresenter[listOfFiles.length];

        for (int i = 0; i < stringFileRepresenters.length; i++){
            stringFileRepresenters[i] = fillInStringFileRepresenter(listOfFiles[i].getName(),listOfFiles[i].getAbsolutePath(),listOfFiles[i].getParent());
        }
        return stringFileRepresenters;
    }

    private boolean searchForMatchDirectoryToOpen(String directoryToOpen) throws Exception {
        File[] roots = rootInitializationModel.initializationOfRootFiles();
        for (File file: roots){
            if (file.getAbsolutePath().equals(directoryToOpen)){
                return true;
            }
        }
        throw new Exception("File was not found");
    }
}
