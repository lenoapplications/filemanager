package file_manager.file_parts.utilz.folder_model_creator;

import file_manager.file_objects.file_objects_types.directory.Directory;
import file_manager.file_objects.file_objects_types.root.Root;
import file_manager.file_parts.utilz.string_represent_file_handler.string_file_representer.StringFileRepresenter;
import file_manager.object_forms.abstractions.file_object_abstract.folders.FolderAbstract;

import java.awt.*;
import java.io.File;

public class FolderModelCreator {
    private static FolderModelCreator fileHandler;

    private FolderModelCreator(){
    }
    public static FolderModelCreator getFileHandler(){
        if (fileHandler == null){
            fileHandler = new FolderModelCreator();
        }
        return fileHandler;
    }

    public FolderAbstract createFolderModel(String absolutePath) throws Exception {
        File file = new File(absolutePath);

        if (file.isDirectory()){
            return setupFolderModel(file,absolutePath);
        }
        throw new Exception(absolutePath.concat(" is not folder"));
    }

    private FolderAbstract setupFolderModel(File file,String absolutePath) throws Exception {
        StringFileRepresenter stringFileRepresenter = new StringFileRepresenter();
        stringFileRepresenter.setParentAbsoultePath( identifyParent(file.getParent()) );
        stringFileRepresenter.setAbsolutePathString(absolutePath);

        return new Directory(stringFileRepresenter);
    }
    private String identifyParent(String parent) throws Exception {
        try{
            boolean bool= parent == Root.rootIdentification;
            System.out.println("PROVJERAVAM   ->   "+ bool+"    file parent je "+parent +"     "+Root.rootIdentification);
            return ( parent.equals(Root.rootIdentification) )? null : parent;
        }catch (Exception e){
            System.out.println("error je paret je null vjerojatno -> "+parent);
            return null;
        }
    }
}
