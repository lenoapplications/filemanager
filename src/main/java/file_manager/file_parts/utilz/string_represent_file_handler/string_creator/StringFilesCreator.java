package file_manager.file_parts.utilz.string_represent_file_handler.string_creator;

import file_manager.file_parts.utilz.string_represent_file_handler.string_file_representer.StringFileRepresenter;

import java.io.File;
import java.util.Date;

public class StringFilesCreator {
    private static StringFilesCreator stringFilesCreator;

    private StringFilesCreator(){
    }

    public static StringFilesCreator getStringFilesCreator() {
        if (stringFilesCreator == null){
            stringFilesCreator = new StringFilesCreator();
        }
        return stringFilesCreator;
    }

    public String createOneStringOfFilesName(StringFileRepresenter[] stringFileRepresenters) {
        StringBuilder stringBuilder = new StringBuilder();

        for (StringFileRepresenter stringFileRepresent: stringFileRepresenters){
            //char fileMark = new File(stringFileRepresent.getAbsolutePath()).isDirectory()?'<':'>';
            //stringBuilder.append(fileMark);
            //Mozda u stringFileRepresenterstavitidi odmah jel directory ili nije
            stringBuilder.append(stringFileRepresent.getFileName());
            stringBuilder.append("\n");
        }
        return stringBuilder.toString();
    }
    public String createOneStringOfFileAbsolutePath(StringFileRepresenter[] stringFileRepresenters){
        StringBuilder stringBuilder = new StringBuilder();

        for (StringFileRepresenter stringFileRepresent: stringFileRepresenters){
            stringBuilder.append(stringFileRepresent.getAbsolutePath());
            stringBuilder.append("\n");
        }
        return stringBuilder.toString();
    }

    public String createStringFromFileProperties(StringFileRepresenter stringFileRepresenter){
        String template = "Absolute path:%s , Extension:%s , File size:%s , Read:%s, Write:%s  ,Last modified :%s";
        File file = new File(stringFileRepresenter.getAbsolutePath());
        String absolutePath = stringFileRepresenter.getAbsolutePath();
        String extension = stringFileRepresenter.getFileExtension();
        String fileSize = getFileSize(file.length());
        boolean canRead = file.canRead();
        boolean canWrite = file.canWrite();
        Date lastModified = new Date(file.lastModified());

        return String.format(template,absolutePath,extension,fileSize,canRead,canWrite,lastModified);
    }

    private String getFileSize(long size){
        String template = "%d %s";
        if ( size < 1000){
            return String.format(template,size,"bytes");
        }else if (size >= 1000000){
            return String.format(template,size / 1000000,"MB");
        }else{
            return String.format(template,size / 1000, "kilobyte");
        }
    }

    public String createNewAbsolutePath(String absolutePath,String newExtension){
        return absolutePath.concat("\\").concat(newExtension);
    }

}
