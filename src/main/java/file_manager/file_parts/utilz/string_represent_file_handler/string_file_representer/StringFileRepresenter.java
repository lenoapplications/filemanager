package file_manager.file_parts.utilz.string_represent_file_handler.string_file_representer;

import java.io.File;

public class StringFileRepresenter {

    private String absolutePath;
    private String parentAbsoultePath;

    public String getAbsolutePath() {
        return absolutePath;
    }

    public void setAbsolutePathString(String absolutePath) {
        this.absolutePath = absolutePath;
    }

    public String getFileName(){
        File file = new File(absolutePath);
        String markFile = file.isDirectory() ? "<" : ">";
        String name = file.getName();
        return (name.length() == 0) ? markFile.concat(absolutePath) : markFile.concat(name);
    }
    public String getCleanFileName(){
        String name = new File(absolutePath).getName();
        return (name.length() == 0) ? absolutePath : name;
    }

    public String getFileExtension(){
        int index = absolutePath.lastIndexOf('.');
        return absolutePath.substring(index + 1);
    }


    public String getParentAbsoultePath() {
        return parentAbsoultePath;
    }

    public void setParentAbsoultePath(String parentAbsoultePath) {
        this.parentAbsoultePath = parentAbsoultePath;
    }


    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("File name : ");
        stringBuilder.append(getFileName());
        stringBuilder.append("\n");
        stringBuilder.append("Absolute path : ");
        stringBuilder.append(absolutePath);
        stringBuilder.append("\n");
        return stringBuilder.toString();
    }
}
