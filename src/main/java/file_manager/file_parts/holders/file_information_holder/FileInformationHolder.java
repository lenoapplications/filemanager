package file_manager.file_parts.holders.file_information_holder;

import file_manager.file_parts.utilz.string_represent_file_handler.string_file_representer.StringFileRepresenter;

public class FileInformationHolder {

    private final StringFileRepresenter stringFileRepresenter;

    public FileInformationHolder(StringFileRepresenter stringFileRepresenter){
        this.stringFileRepresenter = stringFileRepresenter;
    }

    public StringFileRepresenter getStringFileRepresenter() {
        return stringFileRepresenter;
    }
}
