package file_manager.file_parts.string_represent_file_handler.string_creator;

import file_manager.file_parts.utilz.string_represent_file_handler.string_creator.StringFilesCreator;
import file_manager.file_parts.utilz.string_represent_file_handler.string_file_representer.StringFileRepresenter;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import static org.assertj.core.api.Assertions.*;

public class StringFilesCreatorTest {

    private StringFilesCreator stringFilesCreator;
    private StringFileRepresenter[] stringFileRepresenters;

    @Before
    public void before(){
        File[] files = new File(System.getProperty("user.dir")).listFiles();
        stringFileRepresenters = new StringFileRepresenter[files.length];
        stringFilesCreator = StringFilesCreator.getStringFilesCreator();
        for (int i = 0; i < files.length; i++){
            stringFileRepresenters[i]= new StringFileRepresenter();
            stringFileRepresenters[i].setAbsolutePathString(files[i].getAbsolutePath());
        }
    }

    @Test
    public void checkIfStrigFilesCreatorAppropriatlyMakesString(){
        String stringToTest = stringFilesCreator.createOneStringOfFilesName(stringFileRepresenters);

        String[] stringArrayToTest = stringToTest.split("\n");

        for (int i = 0; i< stringArrayToTest.length; i++){
            assertThat(stringArrayToTest[i]).isEqualTo(stringFileRepresenters[i].getFileName());
        }
    }
}
