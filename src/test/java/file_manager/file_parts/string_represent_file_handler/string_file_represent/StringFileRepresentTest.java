package file_manager.file_parts.string_represent_file_handler.string_file_represent;

import file_manager.file_parts.utilz.string_represent_file_handler.string_file_representer.StringFileRepresenter;
import org.assertj.core.api.Condition;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.*;

public class StringFileRepresentTest {

    private StringFileRepresenter stringFileRepresent;

    @Before
    public void before(){
        stringFileRepresent = new StringFileRepresenter();
        stringFileRepresent.setAbsolutePathString("C:\\");
    }

    @Test
    public void checkIfStringFileRepresentTestSavesPaths() {
        assertThat(stringFileRepresent.toString()).isEqualTo("File name : C:\\\nAbsolute path : C:\\\n");
    }
}
