package file_manager.complete_app_test;

import file_manager.file_parts.utilz.string_represent_file_handler.string_creator.StringFilesCreator;
import file_manager.manager.FileManager;
import file_manager.object_forms.models.user_implementation_models.root.RootInitializationModel;
import org.junit.Before;
import static org.assertj.core.api.Assertions.*;


import java.io.File;
import java.util.Scanner;

public class CompleteAppTest {

    private FileManager fileManager;

    @Before
    public void before() throws Exception {

        fileManager = new FileManager();
        fileManager.initFileManager(new RootInitializationModel() {
            @Override
            public File[] initializationOfRootFiles() {
                return File.listRoots();
            }

            @Override
            public String getRootIdentification() {
                return null;
            }
        });
    }


    public static void CheckIfAppWorksCorrectly() throws Exception {
        FileManager fileManager = new FileManager();
        fileManager = new FileManager();
        fileManager.initFileManager(new RootInitializationModel() {
            @Override
            public File[] initializationOfRootFiles() {
                return File.listRoots();
            }

            @Override
            public String getRootIdentification() {
                return null;
            }
        });
        Scanner scanner = new Scanner(System.in);
        String line;
        File parent = null;
        File active;
        System.out.println("Welcome to file explorer");
        while(!(line = scanner.nextLine()).equals("exit")){
            String action = line.substring(0,line.indexOf("<"));
            String fileName = line.substring(line.indexOf("<")+1);

            if (action.equals("open")){
                fileManager.clickedOnFileModel(fileName);
                //assertThat(fileManager.getActiveFolderModel().getFileInformationHolder().getStringFileRepresenter().getFileName()).isEqualTo(fileName);
                System.out.println(fileManager.getActiveFolderModel().getFileInformationHolder().getStringFileRepresenter().getParentAbsoultePath());
               /* parent = new File(fileManager.getActiveFolderModel().getFileInformationHolder().getStringFileRepresenter().getParentAbsoultePath());
                active = new File(fileManager.getActiveFolderModel().getFileInformationHolder().getStringFileRepresenter().getAbsolutePath());
                assertThat(active.getParent()).isEqualTo(parent.getAbsolutePath());*/
            }else if (action.equals("close")){
                fileManager.goBackToParentFileModel();
               /* assertThat(parent.getAbsolutePath()).isEqualTo(fileManager.getActiveFolderModel().getFileInformationHolder().getStringFileRepresenter().getAbsolutePath());
                active = new File(fileManager.getActiveFolderModel().getFileInformationHolder().getStringFileRepresenter().getAbsolutePath());
                parent = new File(fileManager.getActiveFolderModel().getFileInformationHolder().getStringFileRepresenter().getParentAbsoultePath())*/;
            }else if (action.equals("print")){
                System.out.println(StringFilesCreator.getStringFilesCreator().createOneStringOfFilesName(fileManager.getActiveFolderModel().getAllFilesAndFolders()));
            }else if (action.equals("properties")){
                System.out.println(fileManager.getFileProperties(fileName));
            }
        }
    }

    public static void main(String[] args){
        try {
            CheckIfAppWorksCorrectly();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
