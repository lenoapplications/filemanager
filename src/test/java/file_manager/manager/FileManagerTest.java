package file_manager.manager;

import file_manager.file_objects.file_objects_types.directory.Directory;
import file_manager.file_parts.utilz.folder_model_creator.FolderModelCreator;
import file_manager.file_parts.utilz.string_represent_file_handler.string_creator.StringFilesCreator;
import file_manager.file_parts.utilz.string_represent_file_handler.string_file_representer.StringFileRepresenter;
import file_manager.object_forms.abstractions.file_object_abstract.folders.FolderAbstract;
import file_manager.object_forms.models.user_implementation_models.root.RootInitializationModel;
import org.assertj.core.api.Condition;
import org.junit.Test;

import java.io.File;

import static file_manager.static_fields.file_objects_fields.GeneralFileObjectsFields.ROOT_PATH;
import static org.assertj.core.api.Assertions.*;

public class FileManagerTest {

    private FileManager fileManager;



    @Test
    public void initFileManager() throws Exception {
        RootInitializationModel rootInitializationModel = new RootInitializationModel() {
            @Override
            public File[] initializationOfRootFiles() {
                return File.listRoots();
            }

            @Override
            public String getRootIdentification() {
                return null;
            }
        };
        fileManager = new FileManager();
        fileManager.initFileManager(rootInitializationModel);
        FolderAbstract getRootAsCurrent = fileManager.getActiveFolderModel();

        assertThat(getRootAsCurrent.getFileInformationHolder().getStringFileRepresenter().getFileName()).isEqualTo(ROOT_PATH);
    }
    @Test
    public void checkRootChildrens() throws Exception {
        RootInitializationModel rootInitializationModel = new RootInitializationModel() {
            @Override
            public File[] initializationOfRootFiles() {
                return File.listRoots();
            }

            @Override
            public String getRootIdentification() {
                return null;
            }
        };
        fileManager = new FileManager();
        fileManager.initFileManager(rootInitializationModel);

        FolderAbstract root = (FolderAbstract) fileManager.getActiveFolderModel();
        File[] files = File.listRoots();
        StringFileRepresenter[] filesToTest = root.getAllFilesAndFolders();

        assertThat(files.length).isEqualTo(filesToTest.length);

        for (int i = 0; i < filesToTest.length; i++){
            assertThat(files[i].getAbsolutePath()).isEqualTo(filesToTest[i].getAbsolutePath());
        }
    }

    @Test
    public void checkIfRootParentReturnRoot() throws Exception {
        RootInitializationModel rootInitializationModel = new RootInitializationModel() {
            @Override
            public File[] initializationOfRootFiles() {
                return File.listRoots();
            }

            @Override
            public String getRootIdentification() {
                return null;
            }
        };
        fileManager = new FileManager();
        fileManager.initFileManager(rootInitializationModel);

        FolderAbstract previous = fileManager.getPreviousFolderModel();
        FolderAbstract current = fileManager.getActiveFolderModel();
        assertThat(current).isEqualTo(previous);
    }

    @Test
    public void checkIfFolderIsProperlyOpen() throws Exception {
        File file = new File(System.getProperty("user.dir"));
        StringFileRepresenter fileRepresenter = new StringFileRepresenter();
        fileRepresenter.setParentAbsoultePath(file.getParent());
        fileRepresenter.setAbsolutePathString(file.getAbsolutePath());

        Directory directory = new Directory(fileRepresenter);
        fileManager = new FileManager();
        fileManager.testFunction(directory);
        fileManager.clickedOnFileModel(".gradle");

        assertThat(fileManager.getPreviousFolderModel().getFileInformationHolder().getStringFileRepresenter().getFileName()).isEqualTo(file.getName());
    }
    @Test
    public void listFilesOfNewlyOpendFolder() throws Exception {
        File file = new File(System.getProperty("user.dir"));
        fileManager = new FileManager();
        FolderModelCreator folderModelCreator = FolderModelCreator.getFileHandler();
        Directory directory = (Directory) folderModelCreator.createFolderModel(file.getAbsolutePath());
        fileManager.testFunction(directory);
        fileManager.clickedOnFileModel("src");

        assertThat(fileManager.getPreviousFolderModel().getFileInformationHolder().getStringFileRepresenter().getFileName()).isEqualTo(file.getName());

        fileManager.clickedOnFileModel("main");

        assertThat(fileManager.getPreviousFolderModel().getFileInformationHolder().getStringFileRepresenter().getFileName()).isEqualTo("src");

        fileManager.clickedOnFileModel("java");

        assertThat(fileManager.getPreviousFolderModel().getFileInformationHolder().getStringFileRepresenter().getFileName()).isEqualTo("main");

        fileManager.clickedOnFileModel("file_manager");

        assertThat(fileManager.getPreviousFolderModel().getFileInformationHolder().getStringFileRepresenter().getFileName()).isEqualTo("java");

        FolderAbstract folderAbstract = fileManager.getActiveFolderModel();
        String listOfFiles = StringFilesCreator.getStringFilesCreator().createOneStringOfFilesName(folderAbstract.getAllFilesAndFolders());

        assertThat(listOfFiles.split("\n")).are(new Condition<String>() {
            @Override
            public boolean matches(String value) {
                return (value.matches("(file_objects|file_parts|manager|object_forms|static_fields)"));
            }
        });
    }
    @Test
    public void goBackToRootFromSrc() throws Exception {
        String root = "ShareApp";
        File file = new File(System.getProperty("user.dir"));
        fileManager = new FileManager();
        FolderModelCreator folderModelCreator = FolderModelCreator.getFileHandler();
        Directory directory = (Directory) folderModelCreator.createFolderModel(file.getAbsolutePath());
        fileManager.testFunction(directory);
        fileManager.clickedOnFileModel("src");

        assertThat(fileManager.getPreviousFolderModel().getFileInformationHolder().getStringFileRepresenter().getFileName()).isEqualTo(file.getName());

        fileManager.clickedOnFileModel("main");


        assertThat(fileManager.getPreviousFolderModel().getFileInformationHolder().getStringFileRepresenter().getFileName()).isEqualTo("src");

        fileManager.goBackToParentFileModel();
        fileManager.goBackToParentFileModel();
        fileManager.goBackToParentFileModel();

        assertThat(fileManager.getPreviousFolderModel().getFileInformationHolder().getStringFileRepresenter().getFileName()).isEqualTo(root);

    }



}
