package file_manager.manager.position_manager;

import file_manager.file_objects.file_objects_actions.root_actions.RootActions;
import file_manager.file_objects.file_objects_types.root.Root;
import file_manager.object_forms.abstractions.file_object_abstract.folders.FolderAbstract;
import file_manager.object_forms.models.user_implementation_models.root.RootInitializationModel;
import org.junit.Before;
import org.junit.Test;

import static file_manager.static_fields.file_objects_fields.GeneralFileObjectsFields.ROOT_PATH;
import static org.assertj.core.api.Assertions.*;
import java.io.File;

public class PositionManagerTest {

    private PositionManager positionManager;


    @Before
    public void before() throws Exception {
        FolderAbstract root = RootActions.getRootActions().init(new RootInitializationModel() {
            @Override
            public File[] initializationOfRootFiles() {
                return File.listRoots();
            }

            @Override
            public String getRootIdentification() {
                return null;
            }
        }) ;
        positionManager = new PositionManager((Root) root);
        positionManager.setCurrentFileModelAbstract(root);
    }

    @Test
    public void checkIfCurrentPositionMatchesRootPosition(){
        FolderAbstract currentPosition = positionManager.getCurrentFolderModel();
        assertThat(currentPosition.getFileInformationHolder().getStringFileRepresenter().getAbsolutePath()).isEqualTo(ROOT_PATH);
    }
}
