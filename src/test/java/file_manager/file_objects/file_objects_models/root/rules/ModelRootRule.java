package file_manager.file_objects.file_objects_models.root.rules;

import file_manager.object_forms.models.user_implementation_models.root.RootInitializationModel;
import org.junit.rules.ExternalResource;

import java.io.File;

public class ModelRootRule extends ExternalResource {

    private RootInitializationModel rootInitializationModel;

    @Override
    protected void before() throws Throwable {
        rootInitializationModel = new RootInitializationModel() {

            @Override
            public File[] initializationOfRootFiles() {
                return File.listRoots();
            }

            @Override
            public String getRootIdentification() {
                return null;
            }
        };
    }

    public RootInitializationModel getRootInitializationModel() {
        return rootInitializationModel;
    }
}
