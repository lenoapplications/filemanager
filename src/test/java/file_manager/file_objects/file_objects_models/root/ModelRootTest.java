package file_manager.file_objects.file_objects_models.root;

import file_manager.file_objects.file_objects_actions.root_actions.RootActions;
import file_manager.file_objects.file_objects_models.root.rules.ModelRootRule;
import file_manager.file_objects.file_objects_types.root.Root;
import file_manager.object_forms.abstractions.file_object_abstract.folders.FolderAbstract;
import file_manager.object_forms.models.file_object_models.folders.FoldersModel;
import file_manager.object_forms.models.user_implementation_models.root.RootInitializationModel;
import org.assertj.core.api.Condition;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.io.File;

import static file_manager.static_fields.file_objects_fields.GeneralFileObjectsFields.ROOT_NAME;
import static file_manager.static_fields.file_objects_fields.GeneralFileObjectsFields.ROOT_PATH;
import static org.assertj.core.api.Assertions.*;

public class ModelRootTest {

    private RootActions rootActions;

    @Rule
    public ModelRootRule modelRootRule = new ModelRootRule();

    @Before
    public void before(){
        rootActions = RootActions.getRootActions();
    }

    @Test
    public void checkIfObjectIsOfTypeFileModel() throws Exception {

    }


    @Test
    public void checkIfListOfRootFilesIsCorrect() throws Exception {

        FolderAbstract fileModel = (FolderAbstract) rootActions.init(new RootInitializationModel() {
            @Override
            public File[] initializationOfRootFiles() {
                return File.listRoots();
            }

            @Override
            public String getRootIdentification() {
                return null;
            }
        });
        String path = fileModel.getFileInformationHolder().getStringFileRepresenter().getFileName();
        assertThat(path.equals(ROOT_PATH)).isEqualTo(true);
    }



}
