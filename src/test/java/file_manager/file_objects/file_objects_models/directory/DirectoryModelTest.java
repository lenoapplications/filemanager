package file_manager.file_objects.file_objects_models.directory;

import static org.assertj.core.api.Assertions.*;

import file_manager.file_objects.file_objects_types.directory.Directory;
import file_manager.file_parts.utilz.string_represent_file_handler.string_creator.StringFilesCreator;
import file_manager.file_parts.utilz.string_represent_file_handler.string_file_representer.StringFileRepresenter;
import org.assertj.core.api.Condition;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.Arrays;

public class DirectoryModelTest {

    private Directory directory;

    @Before
    public void before(){
        StringFileRepresenter stringFileRepresenter = new StringFileRepresenter();
        File file = new File(System.getProperty("user.dir"));
        stringFileRepresenter.setAbsolutePathString(file.getAbsolutePath());
        stringFileRepresenter.setParentAbsoultePath(file.getParent());
        directory = new Directory(stringFileRepresenter);
    }

}
