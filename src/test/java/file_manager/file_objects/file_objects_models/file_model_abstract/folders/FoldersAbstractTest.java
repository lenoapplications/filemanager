package file_manager.file_objects.file_objects_models.file_model_abstract.folders;

import file_manager.file_objects.file_objects_actions.root_actions.RootActions;
import file_manager.file_objects.file_objects_types.directory.Directory;
import file_manager.file_parts.utilz.string_represent_file_handler.string_creator.StringFilesCreator;
import file_manager.file_parts.utilz.string_represent_file_handler.string_file_representer.StringFileRepresenter;
import file_manager.object_forms.abstractions.file_object_abstract.folders.FolderAbstract;
import file_manager.object_forms.models.user_implementation_models.root.RootInitializationModel;
import org.junit.Test;

import java.io.File;

import static org.assertj.core.api.Assertions.*;

public class FoldersAbstractTest {

    private FolderAbstract foldersAbstract;



    @Test
    public void checkIfChildrensOfFParentRootAreCorrect() throws Exception {
        foldersAbstract = (FolderAbstract) RootActions.getRootActions().init(new RootInitializationModel() {
            @Override
            public File[] initializationOfRootFiles() {
                return File.listRoots();
            }

            @Override
            public String getRootIdentification() {
                return null;
            }
        });

        StringFileRepresenter[] stringFileRepresenters = foldersAbstract.getAllFilesAndFolders();
        File[] files = File.listRoots();

        String[] splittedFiles = StringFilesCreator.getStringFilesCreator().createOneStringOfFileAbsolutePath(stringFileRepresenters).split("\n");

        for ( int i = 0; i< splittedFiles.length ; i ++){
            assertThat(splittedFiles[i]).isEqualTo(files[i].getAbsolutePath());
        }

    }
    @Test
    public void checkIfChildrensOfParentDirectoryAreCorrect(){
        File file = new File(System.getProperty("user.dir"));
        StringFileRepresenter fileRepresenter = new StringFileRepresenter();
        fileRepresenter.setAbsolutePathString(file.getAbsolutePath());
        fileRepresenter.setParentAbsoultePath(file.getParent());
        foldersAbstract = new Directory(fileRepresenter);

        StringFileRepresenter[] stringFileRepresenters = foldersAbstract.getAllFilesAndFolders();
        File[] files = new File(foldersAbstract.getFileInformationHolder().getStringFileRepresenter().getAbsolutePath()).listFiles();

        String[] splittedFiles = StringFilesCreator.getStringFilesCreator().createOneStringOfFilesName(stringFileRepresenters).split("\n");

        for ( int i = 0; i< files.length ; i ++){
            assertThat(splittedFiles[i]).isEqualTo(files[i].getName());
        }

    }
}
