package file_manager.file_objects.file_objects_models.file_model_abstract;

import file_manager.file_parts.utilz.string_represent_file_handler.string_file_representer.StringFileRepresenter;
import file_manager.object_forms.abstractions.file_object_abstract.folders.FolderAbstract;
import org.junit.Before;
import org.junit.Test;

public class FileModelAbstractTest {
    private FolderAbstract fileModelAbstract;

    @Before
    public void before(){
        StringFileRepresenter stringFileRepresenter = new StringFileRepresenter();
        stringFileRepresenter.setAbsolutePathString("C:\\");
        fileModelAbstract = new FolderAbstract(stringFileRepresenter) {

            @Override
            public String open(String directoryToOpen) throws Exception {
                return null;
            }

            @Override
            public StringFileRepresenter[] getAllFilesAndFolders() {
                return new StringFileRepresenter[0];
            }
        };
    }

    @Test
    public void getCurrentStringPathToFile(){

    }

}
