package file_manager.file_objects.file_objects_action.root.initialization;


import file_manager.file_objects.file_objects_actions.root_actions.RootActions;
import file_manager.file_objects.file_objects_actions.root_actions.initialization_action.RootInitializationActions;
import file_manager.object_forms.models.user_implementation_models.root.RootInitializationModel;
import org.assertj.core.api.Condition;
import org.hamcrest.core.StringContains;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.File;


import static org.assertj.core.api.Assertions.*;

@RunWith(MockitoJUnitRunner.class)
public class RootInitializationTest {

    private RootActions rootAction;
    private RootInitializationModel rootInitializationModel;


    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void before(){
        rootAction = RootActions.getRootActions();
    }


    @Test
    public void RootInitializationTest_expectedExceptionFileIsNotRoot() throws Exception {
        RootInitializationActions actions = new RootInitializationActions();
        rootInitializationModel = new RootInitializationModel() {
            @Override
            public File[] initializationOfRootFiles() {
                return new File(System.getProperty("user.dir")).listFiles();
            }

            @Override
            public String getRootIdentification() {
                return null;
            }
        };
        expectedException.expect(Exception.class);
        expectedException.expectMessage(StringContains.containsString("File is not root :"));
        actions.initializingRootFilesAction(rootInitializationModel);
    }

    @Test
    public void RootInitializationTest_checkIfRootFilesAreReturned() throws Exception {
        rootInitializationModel = new RootInitializationModel() {
            @Override
            public File[] initializationOfRootFiles() {
                return File.listRoots();
            }

            @Override
            public String getRootIdentification() {
                return null;
            }
        };
        assertThat((rootInitializationModel.initializationOfRootFiles())).are(new Condition<File>() {
            @Override
            public boolean matches(File value) {
                return value.getParent() == null;
            }
        });
    }
}
